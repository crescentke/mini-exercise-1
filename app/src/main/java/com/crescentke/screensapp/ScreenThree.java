package com.crescentke.screensapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class ScreenThree extends AppCompatActivity {

    private Button back;
    private ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_three);

        back = (Button)(findViewById(R.id.btn_back));
        img = (ImageView)(findViewById(R.id.img_viewed));

        img.setImageResource(R.drawable.flash);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
