package com.crescentke.screensapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button img1;
    private Button img2;
    private Button img3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        img1 = (Button)(findViewById(R.id.btn_arrow));
        img2 = (Button)(findViewById(R.id.btn_flash));
        img3 = (Button)(findViewById(R.id.btn_originals));



        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchScreenOne();
            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchScreenTwo();
            }
        });

        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchScreenThree();
            }
        });
    }

    private void launchScreenThree(){
        Intent intent = new Intent(this, ScreenFour.class);
        startActivity(intent);
    }
    private void launchScreenTwo(){
        Intent intent = new Intent(this, ScreenThree.class);
        startActivity(intent);
    }
    private void launchScreenOne(){
        Intent intent = new Intent(this, ScreenTwo.class);
        startActivity(intent);
    }
}
